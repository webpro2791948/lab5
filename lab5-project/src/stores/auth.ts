import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'jirapat325@gmail.com',
    password: '325',
    fullName: 'Jirapat Thitiworn',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
